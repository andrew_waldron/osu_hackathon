var express = require('express');
var server = express();

server.use('/', express.static('web'));
server.use('/node_modules', express.static('node_modules'));

// app.get('/api/', function (req, res) {
//   res.send('Hello World!');
// });

server.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
