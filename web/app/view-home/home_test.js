'use strict';

describe('hackathon.home module', function() {
  beforeEach(module('hackathon.home'));

  describe('view1 controller', function() {
    it('Should be amazing', inject(function($controller) {
      var homeController = $controller('HomeController');
      expect(homeController).toBeDefined();
    }));
  });
});