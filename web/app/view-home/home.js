'use strict';

angular.module('hackathon.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'app/view-home/home.htm',
    controller: 'HomeController'
  });
}])

.controller('HomeController', [function() {

}]);