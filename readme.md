Trello Board
===

The following is the trello board: https://trello.com/b/teig54fw

Problems to solve:
===

The biggest problems were definitely registration and judging.

 + *Registration* - Big thing here to help with is logistics. One possible solution app that will help with the sign in process. That process has:
    - enter simple data
    - in person day of registration
    - We would like to have to other new things with registration
        1: ability to register an entire team at once rather than as individuals
        2: ability to have a better way to handle the walk ins. Especially with regards to users who are just going to learn to start coding. We might consider separating those types of people out in a way that will make it very clear they are not entering to be judged and they are not going to deliver anything.
    - get the name and match up with registration. One issue is not everyone that was registered was actually registered
    - right now they have two online phases. 
        1: Collect shirt size and basic information
        2: additional information collected that was missed the first time around
 + *Mentors*
    - Display a list of the mentors and what skills they know. This would be simple and help.
    - Schedule a mentor
 + *Judging Issues*
    - pair judge with team
    - simplify judging process
    - find a way to determine who wants to actually be judged and is ready to be judged
    - Timed judging to keep flow going well and time management for judging
    - We need to judge the business value of the app as well as the technical quality of the code and solution. This goes with the "project diversity" bullet as well.
    - Faculty and sponsor tiers are some of the judges.
    - Clarify the criteria around judging. Examples: creativity, implementation, business value, etc
    - Want to make sure they get this done so that the winnners and runner up get judged and announced at the event. Getting this done later isn't at all the same
    - Possibly consider two separate evaluations: Business evaluation and technical evaluation
 + *Auditing Code* - track whether or not they have actually started the project before the event. We could possibly do better at this.
    - difficult, but we could audit the day of the event. Talk about the code and make sure they understand it (didn't copy)
    - github or something else could help
    + check license agreements as well
 + *Project Diversity* - One other problem is how to compare and judge across projects that are very different in nature. One goal here is they need to have a way to make sure that there is a technical resource judging every project. This should help to alleviate issues caused by complex things being done on prebuilt platforms vs building a brand new platform from scratch.
 + *Announcements*
    - Advertising other activities and annoucements is done via twitter, blast and via loudspeaker
    - This year the event is more spread out in the building so the adverts need to be better than last year
 + *Team Management*
    - IF we don't have teams register then we should have a better way to form and know about teams. Right now it has been basically self registered.
    - They could have a pre-event to find a team that is working on a project.
    - Tried to round up people with no team and this didn't end up happening.
    - team size is 2 - 4 persons right now
    - Advert could also be by role or what skills they are looking for
 + *Feedback*
    - get real time feedback during the event
    - basic idea is could use twitter or a hash tag during the event. Tweet to a particular account during the event if there are issues

Last Year Hackathon Statistics
===
  + 1000+ registered (round 1)
  + ~800 registered (round 2)
  + ~500 on the day of the event (50 - 60% attendance rate)

Event date is november 17 or 19. Registration will be sometime in Sept. Don't have a cut off date but then this didn't happen. Two things is after a date or after a specific number of registrants would like to cut if off.

Notes verbatim from the board (Ordered list)
===

1. judging:
    - structure for judging
    - matching judges to teams
    - technical vs business value of the app
1. Pre-registration
1. Simple fast sign in

     ------------------------------------------------------

1. identify mentor expertise
1. paramters as to what types of apps. This comes down to rules and categories for judging.
1. itinerary of breaks and announcements
1. direct team communication

Solutions:
===

Things we want from an application mobile, web or otherwise:

 - Online registration. Waiver - digitally sign or with captcha make the waiver valid
 - real time feedback
 - barcode reading on sign in, judging, etc
 - dicrect connection to participants
 - push notifications
 - schedule updates
 - request for mentor assistance and show when mentor is available
 - schedule and guide judging. Opt in and out for judging

 There also needs to be a portal for staff and judging. These two portals are going to be administrative and via access.xx`
